## ugg-user 7.1.2 N2G47H V11.0.2.0.NDKMIXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: ulova
- Brand: Xiaomi
- Flavor: revengeos_ulova-userdebug
- Release Version: 11
- Id: RD2A.211001.002
- Incremental: f049d3c237
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: xiaomi/ugg/ugg:7.1.2/N2G47H/V11.0.2.0.NDKMIXM:user/release-keys
- OTA version: 
- Branch: ugg-user-7.1.2-N2G47H-V11.0.2.0.NDKMIXM-release-keys
- Repo: xiaomi_ulova_dump_26124


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
